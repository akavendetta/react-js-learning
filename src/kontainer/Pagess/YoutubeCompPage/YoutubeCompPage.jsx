import React, { Component, Fragment } from 'react';
import YouTubeComp from '../../../component/YouTubeComponent/YoutubeComponent';
import { connect } from 'react-redux';

class YotubeCompPage extends Component {
    render() {
        return (
            <Fragment>
                <p>Youtube Component</p>
                <hr />
                <br />
                <YouTubeComp time="12.4" title="React Basic" desc="20x ditonton. 2 hari yang lalu" />
                <YouTubeComp time="10.2" title="React Intermediate" desc="20x ditonton. 2 hari yang lalu" />
                <YouTubeComp time="4.5" title="React Advandce" desc="50k ditonton. 2 hari yang lalu" />
                <YouTubeComp time="7.3" title="React Master" desc="2.5M ditonton. 2 hari yang lalu" />
                <YouTubeComp />
                <hr />
                <br />
                <p>Total Order: {this.props.order} </p>
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        order: state.totalOrder
    }
}
export default connect(mapStateToProps)(YotubeCompPage);