import React, { Component, Fragment } from 'react';
import './Product.css';
import CardProduct from '../Product/CardProduct/CardProduct'
import { connect } from 'react-redux'

class Product extends Component {
    // state = {
    //     order: 0
    // }

    // handleCounterChange = (newValue) => {
    //     this.setState({
    //         order: newValue
    //     })
    // }

    render() {
        return (
            <Fragment>
                <div className="header">
                    <div className="logo">
                        <div className="logoone"></div>
                        {/* <img src="121.jpg" alt=""/> */}
                    </div>
                    <div className="troley">
                        {/* <img src="./121.jpg" alt=""/> */}
                        <div className="count">{this.props.order}</div>
                    </div>
                </div>
                <CardProduct />
                {/* <CardProduct onCounterchange={(value) => this.handleCounterChange(value)} /> */}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        order: state.totalOrder
    }
}

export default connect(mapStateToProps)(Product);