import React, { Component } from 'react';
import { connect } from 'react-redux';

class CardProduct extends Component {
    // state = {
    //     order: 0
    // }

    // handleCounterChange = (newValue) => {
    //     this.props.onCounterchange(newValue);
    // }

    // handlePlus = () => {
    //     this.setState({
    //         order: this.state.order + 1
    //     }, () => {
    //         this.handleCounterChange(this.state.order);
    //     })

    // }
    // handleMin = () => {
    //     if (this.state.order > 0) {
    //         this.setState({
    //             order: this.state.order - 1
    //         }, () => {
    //             this.handleCounterChange(this.state.order);
    //         })
    //     }
    // }
    render() {
        console.log(this.props);

        return (
            <div className="card">
                <div className="img-thumb-prod">
                    <img src="./121.jpg" alt="" />
                </div>
                <p className="product-title">lkjhgf</p>
                <p className="product-price">fcgvhbjk</p>
                <div className="counter">
                    <button className="minus" onClick={this.props.handleMin}>-</button>
                    <input type="text" value={this.props.order} />
                    <button className="plus" onClick={this.props.handlePlus}>+</button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        order: state.totalOrder
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        handlePlus: () => dispatch({ type: 'PLUS_ORDER' }),
        handleMin: () => dispatch({ type: 'MINUS_ORDER' }),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CardProduct);