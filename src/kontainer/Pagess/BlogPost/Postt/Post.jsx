import React from 'react';
const Post = (props) => {
    return (
        <div className="post">
            <div className="imgg-thumb">
                <img src="https://placeimg.com/200/147/tech" alt="dumy" />
            </div>
            <div className="contentt">
                <div className="tittle" onClick={() => props.goDetail(props.data.id)}>{props.data.title}</div>
                <div className="desc">{props.data.body}</div>
                <button className="update" onClick={() => props.update(props.data)} >^</button>
                <button className="remove" onClick={() => props.remove(props.data.id)}>X</button>
            </div>
        </div>
    )
}
export default Post;