import React, { Component, Fragment } from 'react';
import YoutubeCompPage from '../Pagess/YoutubeCompPage/YoutubeCompPage'
import Product from '../Pagess/Product/Product'
import BlogPost from '../Pagess/BlogPost/BlogPost'
import { BrowserRouter, Route, Link } from 'react-router-dom';
import './Home.css';
import DetailPost from '../Pagess/BlogPost/DetailPost/DetailPost';

class Home extends Component {
    render() {
        return (
            <BrowserRouter>
                {/* <div>
                    <p>Youtube Component</p>
                    <hr />
                    <br />
                    <YouTubeComponent time="12.4" title="React Basic" desc="20x ditonton. 2 hari yang lalu" />
                    <YouTubeComponent time="10.2" title="React Intermediate" desc="20x ditonton. 2 hari yang lalu" />
                    <YouTubeComponent time="4.5" title="React Advandce" desc="50k ditonton. 2 hari yang lalu" />
                    <YouTubeComponent time="7.3" title="React Master" desc="2.5M ditonton. 2 hari yang lalu" />
                    <YouTubeComponent />
                    <p>Counter</p>
                    <hr />
                    <Product />
                    <hr />
                    <BlogPost />
                </div> */}
                <Fragment>
                    <div className='header'>
                        <Link to="/">Product</Link>
                        <br />
                        <Link to="/blogpost">Blog Post</Link>
                        <br />
                        <Link to="/youtube">Youtube</Link>
                    </div>
                    <Route path="/" exact component={Product} />
                    <Route path="/detail-post/:postID" component={DetailPost} />
                    <Route path="/blogpost" component={BlogPost} />
                    <Route path="/youtube" component={YoutubeCompPage} />
                </Fragment>
            </BrowserRouter>
        )
    }
}
export default Home;