import React from 'react';
import './YouTubeComponent.css';

const YouTubeComponent = (props) => {
    return (
        <div className="youtube-wrapper">
            <div className="img-thumb">
                <div className="image"></div>
                <div className="time">{props.time}</div>
            </div>
            <p className="title">{props.title}</p>
            <p className="desc">{props.desc}</p>
        </div>
    )

}
YouTubeComponent.defaultProps ={
    time: '00.00',
    title: 'Title Here',
    desc: "xx ditonton. xx hari yang lalu"
}
export default YouTubeComponent;